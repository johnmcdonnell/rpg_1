/*
John McDonnell & Delwyn Monagan
K28_Comp2
Games Development CA 2
RPG Text Based Game
Final Version
Due Date: 22-Nov-2014
*/

// player cpp

#include "Player.h"
#include "Item.h"
#include <iostream>
#include <vector>
using namespace std;


Player::Player()
{
	mName = "Default";
}
Player::Player(string name)
{
	mName = name;
	mNumLives = 5;
	vector<Item> mInventory = vector<Item>(5);
	mCount = 0;
}

std::string Player::getName()
{
	return mName;
}
int Player::getNumLives()
{
	return mNumLives;
}
int Player::getCount()
{
	return mCount;
}

vector<Item> Player::getInventory()
{
	return mInventory;
}


void Player::setName(std::string Name)
{
	mName = Name;
}
void Player::setNumLives(int numLives)
{
	mNumLives = numLives;
}
void Player::setCount(int count)
{
	mCount = count;
}
void Player::addItem(Item i, int count)
{
	if (count < 5)
	{
		mInventory.push_back(i);
	}
}

int Player::removeItem(int position, int count)
{
	mInventory.erase(mInventory.begin() + position);
	return count = count - 1;
}



