/*
John McDonnell & Delwyn Monagan
K28_Comp2
Games Development CA 2
RPG Text Based Game
Final Version
Due Date: 22-Nov-2014
*/

// room cpp
#include "room.h"
#include <iostream>
using namespace std;

// constructor
Room::Room(std::string name, string description, string exits, int position, int north, int south, int east, int west )
{
	mName = name;
	mDescription = description;
	mExits = exits;
	mNorth = north;
	mSouth = south;
	mEast = east;
	mWest = west;
	mPosition = position;
}
//setters
void Room::setName(std::string name)
{
	mName = name;
}
void Room::setNorth(int north)
{
	mNorth = north;
}
void Room::setSouth(int south)
{
	mSouth = south;
}
void Room::setEast(int East)
{
	mEast = East;
}
void Room::setWest(int west)
{
	mWest = west;
}
void Room::setPositon(int position)
{
	mPosition = position;
}
void Room::setItem_ItemName(string item)
{
	mItem.setItemName(item);
}
void Room::setItem_Itemdescription(string itemDescription)
{
	mItem.setItemDescription(itemDescription);
}


//getters
std::string Room::getName()
{
	return mName;
}
int Room::getNorth()
{
	return mNorth;
}
int Room::getSouth()
{
	return mSouth;
}
int Room::getEast()
{
	return mEast;
}
int Room::getWest()
{
	return mWest;
}
int Room::getPositon()
{
	return mPosition;
}


//ROOMS
string Room::getDescription()
{
	return mDescription;
}
string Room::getExits()
{
	return mExits;
}
