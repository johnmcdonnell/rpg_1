/*
John McDonnell & Delwyn Monagan
K28_Comp2
Games Development CA 2
RPG Text Based Game
Final Version
Due Date: 22-Nov-2014
*/

// item cpp

#include <iostream>
#include <vector>
#include "Item.h"
using namespace::std;

// constructor
Item::Item(string itemName, string  itemDescription)
{
	mItemName = itemName;
	mItemDescription = itemDescription;
}

Item::Item()
{
	mItemName = "Default";
	mItemDescription = "";
}


// setters
void Item::setItemName(std::string itemName)
{
	mItemName = itemName;
}

void Item::setItemDescription(std::string itemdescription)
{
	mItemDescription = itemdescription;
}

// getters
std::string Item::getItemName()
{
	return mItemName;
}

std::string Item::getItemDescription()
{
	return mItemDescription;
}

void Item::removeItem()
{
	setItemName("No item");
	setItemDescription(" ");
}