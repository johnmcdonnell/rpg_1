// Item header file
#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <vector>
#include "Item.h"

class Item{
private:
	//variables
	std::string mItemName;
	std::string mItemDescription;
	std::string mName;

public:
	//Constructor
	Item();
	Item(std::string, std::string);

	//setter and getters
	std::string getItemName();
	std::string getItemDescription();


	void setItemName(std::string);
	void setItemDescription(std::string);

	void removeItem();
	//Functions;
};

#endif // Item_H