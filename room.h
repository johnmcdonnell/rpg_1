// Room header file

#ifndef ROOM_H
#define ROOM_H

#include "Item.h"
#include<string>
using namespace std;

class Room
{
public:
	//construtcor
	Room();
	Room(int position);
	Room(int position, string, string);
	Room(std::string, std::string, std::string, int, int, int, int, int);

	//setters
	void setName(std::string);
	void setPositon(int);
	void setNorth(int);
	void setSouth(int);
	void setEast(int);
	void setWest(int);
	void setItem_ItemName(string);
	void setItem_Itemdescription(string);

	//getters
	std::string getName();
	int getNorth();
	int getSouth();
	int getEast();
	int getWest();
	int getPositon();

	std::string getDescription();
	std::string getExits();
	void getItems();
	void removeItem(int item);

private:
	//variables
	std::string mName;
	std::string mDescription;
	std::string mExits;
	int mNorth;
	int mSouth;
	int mEast;
	int mWest;
	int mPosition;
	Item mItem;
};
#endif // Room_H