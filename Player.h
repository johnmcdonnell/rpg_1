//Player header file

#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "Item.h"
#include <vector>

class Player
{
public:
	//Constructor
	Player();
	Player(std::string);

	//Functions
	std::string getName();
	int getNumLives();
	std::vector<Item> getInventory();
	int getCount();

	void setName(std::string mName);
	void setNumLives(int mNumLives);
	void setInventory(std::vector<Item> mInventory);
	void setCount(int mCount);


	void addItem(Item i, int count);
	int removeItem(int position, int count);

private:
	std::string mName;
	int mNumLives;
	std::vector<Item> mInventory;
	int mCount;
};

#endif //End PLAYER_H
