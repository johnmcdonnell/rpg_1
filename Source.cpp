

    #include <stdlib.h>
    #include <time.h>
    #include <iostream>
    #include <fstream>
    #include <string>
    #include <windows.h>
    //used to maximize screen
    #pragma comment(lib,"kernel32.lib")
    #pragma comment(lib,"user32.lib")
    using namespace std;
    int main()
    {
    //tells screen to maximize
    HWND hWnd = GetConsoleWindow();
    ShowWindow(hWnd,SW_MAXIMIZE);
    cout << "********************************************************************************";
    cout << "****************************** ***************** *******************************";
    cout << "**************************** ******************* ****************************";
    cout << "************************* ************************ *************************";
    cout << "********************** **************************** **********************";
    cout << "******************** ****************************** *******************";
    cout << "****************** ******************************** ****************";
    cout << "**************** ****** ** * * * ****** **************";
    cout << "************** ******* ** * *** * * * * * * ******* ************";
    cout << "************* ******* *** * ** * * * * * * ******** ***********";
    cout << "************ ******* *** * *** * * * * * * ******** ***********";
    cout << "*********** ******* ** * *** * * * * * * ******** **********";
    cout << "*********** ****** ** * *** * * * ******* **********";
    cout << "*********** * ********************************** * **********";
    cout << "*********** ** * ****************************** * ** ***********";
    cout << "************ ** * ***************************** ** ** ***********";
    cout << "************* * *** ** ******************************* ******* * ************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "************************* *************************** *******************";
    cout << "**************************** *********************** **********************";
    cout << "******************************* ******************** ************************";
    cout << "********************************* ****************** **************************";
    cout << "******************** ************* **************** ************* **************";
    cout << "********************* ********** ************** ********** ***************";
    cout << "********************* ******* ************ ******* ***************";
    cout << "******************** ********** ********** ********** **************";
    cout << "******************** ****** **** ******** ***** ****** **************";
    cout << "********************* **** *************** **** ***************";
    cout << "*********************** ************* ******************";
    cout << "************************************ ********** ******************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "******************************* ********************** *************************";
    cout << "********************************* ****************** ****** ********************";
    cout << "************************** ******* **************** ******** *******************";
    cout << "************************* *** **** ************** ***** *** ******************";
    cout << "**************************** ********************* *********************";
    cout << "****************************** ******************* ***********************";
    cout << "****************** ************ ***************** *************************";
    cout << "***************** * ************ ***************** ************ *************";
    cout << "************** * *** ************* *************** ************* * * **********";
    cout << "*************** ***** ***** ********************************** *** **********";
    cout << "*************** ******* *** * ********************* ********** **** ***********";
    cout << "************************ * *** ******************* ********* *****************";
    cout << "************************* ***** ************ **** * ******* ******************";
    cout << "************************* ******* ********** * ** *** ***** *******************";
    cout << "************************* ******** ***** ** *** ***** *** ********************";
    cout << "*********************************** *** * **** ******** * *********************";
    cout << "************************************ * * *************** **********************";
    cout << "************************************* *** **************************************";
    cout << "************************************* ******************************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    cout << "********************************************************************************";
    Beep(300,100);
    Beep(100,100);
    Beep(300,100);
    Beep(100,300);
    Beep(300,100);
    Beep(150,100);
    Beep(300,100);
    Beep(100,100);
    Beep(300,100);
    Beep(150,300);
    Beep(350,100);
    Beep(200,100);
    Beep(250,100);
    Beep(150,100);
    Beep(350,100);
    Beep(200,300);
    Beep(300,100);
    Beep(200,100);
    Beep(300,100);
    Beep(150,100);
    Beep(300,100);
    Beep(120,300);
    Beep(300,100);
    Beep(190,100);
    Beep(300,100);
    Beep(140,100);
    Beep(300,100);
    Beep(170,300);
    Beep(360,100);
    Beep(130,100);
    Beep(300,100);
    Beep(120,100);
    Beep(300,100);
    Beep(190,300);
    Beep(300,100);
    Beep(150,100);
    Beep(300,100);
    Beep(150,100);
    Beep(300,100);
    Beep(100,300);
    Beep(150,200);
    Beep(200,200);
    Beep(250,300);
    system("cls");
    int person, opponent, atk, asuccess, msuccess, game = 1, counter1=1, level = 1, atr, a, b, c = 0, d, decision, item;
    //opponent
    int attackop, defenseop, speedop, healthop = 100, opac, expo = 0, moneyo = 0;
    //user
    int magicus, attackus, defenseus, speedus, healthus = 100, exp = 0, money = 0, levelup = 200;
    //names
    string nameus, nameop;
    char exit;
    //classes
    int healus = 0;
    do{
    cout << "(1)\n***************\nThe Wizard:\n attack - 30\n defense - 40\n magic - 60\n speed - 60\n***************" << endl << endl;
    cout << "(2)\n***************\nThe Knight:\n attack - 50\n defense - 75\n magic - 40\n speed - 30\n***************" << endl << endl;
    cout << "(3)\n***************\nThe Berzerker:\n attack - 60\n defense - 50\n magic - 30\n speed - 50\n***************" << endl << endl;
    cout << "(4)\n***************\nThe Banshee:\n attack - 40\n defense - 40\n magic - 50\n speed - 65\n***************" << endl << endl;
    cout << "(5)\n***************\nThe Goblin:\n attack - 60\n defense - 65\n magic - 30\n speed - 40\n***************" << endl << endl;
    cout << "(6)\n***************\nThe Demon:\n attack - 65\n defense - 70\n magic - 25\n speed - 30\n***************" << endl << endl;
    cout << "Which Class would you like to be?: ";
    cin >> person;
    if((person > 6)&&(person != 666))
    {
    system("cls");
    cout << "You must enter an integer between 1 and 6!!!!\n\n";
    system("pause");
    system("cls");
    }
    if(person == 666)
    {
    system("cls");
    cout << "Muahahahaha!! You have unleashed Satan!!!";
    cout << "\n***************\nSatan:\n attack - 100\n defense - 100\n magic - 100\n speed - 100\n***************" << endl << endl;
    Beep(300,200);
    Sleep(100);
    Beep(200,200);
    Sleep(100);
    Beep(100,200);
    Sleep(100);
    Beep(100,100);
    Beep(125,50);
    Beep(150,50);
    Beep(125,50);
    Beep(100,200);
    system("pause");
    system("cls");
    }
    }while((person > 6)&&(person != 666));
    //information for user
    switch(person)
    {
    case 1:
    attackus = 30, defenseus = 40, magicus = 60, speedus = 60, nameus = "Wizard";
    break;
    case 2:
    attackus = 50, defenseus = 60, magicus = 40, speedus = 40, nameus = "Knight";
    break;
    case 3:
    attackus = 60, defenseus = 50, magicus = 30, speedus = 50, nameus = "Berzerker";
    break;
    case 4:
    attackus = 40, defenseus = 40, magicus = 50, speedus = 65, nameus = "Banshee";
    break;
    case 5:
    attackus = 60, defenseus = 65, magicus = 30, speedus = 40, nameus = "Goblin";
    break;
    case 6:
    attackus = 65, defenseus = 70, magicus = 30, speedus = 25, nameus = "Demon";
    break;
    case 666:
    attackus = 100, defenseus = 100, magicus = 100, speedus = 100, nameus = "Satan";
    break;
    default:
    cout << "No One";
    }
    do{
    do{
    system("cls");
    cout << "Hello, " << nameus << ", what would you like to do?\n\n";
    cout << "(1)\n**********\nGo to the Arena\n**********\n\n";
    cout << "(2)\n**********\nGo to the Armory\n**********\n:";
    cin >> decision;
    switch(decision)
    {
    case 1:
    int random3, random4, random5;
    srand ((unsigned)time(0));
    random3 = (rand() % 4) + 1;
    random4 = (rand() % 4) + 1;
    random5 = (rand() % 4) + 1;
    if(healthop == 0)
    healthop = 100;
    system("cls");
    do{
    cout << "Welcome to the arena, " << nameus << ", which opponent would you like to face?" << endl;
    d = (level * -1);
    switch(random3)
    {
    case 1:
    if(level <= 1)
    {
    cout << "(" << d + 2 << ")\n***************\nThe Tree(lvl 1):\n attack - 0\n defense - 20\n speed - 0\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if(level <= 1)
    {
    cout << "(" << d + 2 << ")\n***************\nThe Rock(lvl 1):\n attack - 0\n defense - 30\n speed - 0\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if(level <= 1)
    {
    cout << "(" << d + 2 << ")\n***************\nThe Computer(lvl 1):\n attack - 0\n defense - 10\n speed - 0\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if(level <= 1)
    {
    cout << "(" << d + 2 << ")\n***************\nThe Book(lvl 1):\n attack - 0\n defense - 5\n speed - 0\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    switch(random4)
    {
    case 1:
    if(level <= 2)
    {
    cout << "(" << d + 3 << ")\n***************\nThe Rat(lvl 2):\n attack - 30\n defense - 20\n speed - 30\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if(level <= 2)
    {
    cout << "(" << d + 3 << ")\n***************\nThe Cat(lvl 2):\n attack - 40\n defense - 20\n speed - 30\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if(level <= 2)
    {
    cout << "(" << d + 3 << ")\n***************\nThe Dog(lvl 2):\n attack - 45\n defense - 25\n speed - 30\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if(level <= 2)
    {
    cout << "(" << d + 3 << ")\n***************\nThe Bat(lvl 2):\n attack - 30\n defense - 20\n speed - 35\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    switch(random5)
    {
    case 1:
    if(level <= 3)
    {
    cout<< "(" << d + 4 << ")\n***************\nThe Mage(lvl 3):\n attack - 65\n defense - 35\n speed - 45\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if(level <= 3)
    {
    cout<< "(" << d + 4 << ")\n***************\nThe Jackal(lvl 3):\n attack - 60\n defense - 50\n speed - 50\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if(level <= 3)
    {
    cout<< "(" << d + 4 << ")\n***************\nThe Crusader(lvl 3):\n attack - 65\n defense - 55\n speed - 45\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if(level <= 3)
    {
    cout<< "(" << d + 4 << ")\n***************\nThe Wolf(lvl 3):\n attack - 55\n defense - 45\n speed - 55\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    switch(random3)
    {
    case 1:
    if((level <= 4)&&(level >= 2))
    {
    cout<< "(" << d + 5 << ")\n***************\nThe Assassin(lvl 4):\n attack - 75\n defense - 35\n speed - 55\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if((level <= 4)&&(level >= 2))
    {
    cout<< "(" << d + 5 << ")\n***************\nThe Pacman(lvl 4):\n attack - 65\n defense - 30\n speed - 60\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if((level <= 4)&&(level >= 2))
    {
    cout<< "(" << d + 5 << ")\n***************\nThe Man Eating Bunny(lvl 4):\n attack - 75\n defense - 35\n speed - 55\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if((level <= 4)&&(level >= 2))
    {
    cout<< "(" << d + 5 << ")\n***************\nThe Warlock(lvl 4):\n attack - 70\n defense - 40\n speed - 50\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    switch(random4)
    {
    case 1:
    if((level <= 5)&&(level >= 3))
    {
    cout<< "(" << d + 6 << ")\n***************\nPoseidon(lvl 5):\n attack - 80\n defense - 45\n speed - 45\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if((level <= 5)&&(level >= 3))
    {
    cout<< "(" << d + 6 << ")\n***************\nMinitaur(lvl 5):\n attack - 65\n defense - 55\n speed - 50\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if((level <= 5)&&(level >= 3))
    {
    cout<< "(" << d + 6 << ")\n***************\nThe Cyclops(lvl 5):\n attack - 75\n defense - 45\n speed - 50\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if((level <= 5)&&(level >= 3))
    {
    cout<< "(" << d + 6 << ")\n***************\nThe Dragon(lvl 5):\n attack - 80\n defense - 55\n speed - 30\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    switch(random4)
    {
    case 1:
    if((level <= 6)&&(level >= 4))
    {
    cout<< "(" << d + 7 << ")\n***************\nAthena(lvl 6):\n attack - 85\n defense - 55\n speed - 60\n***************\n";
    cout << endl;
    }
    break;
    case 2:
    if((level <= 6)&&(level >= 4))
    {
    cout<< "(" << d + 7 << ")\n***************\nZeus(lvl 6):\n attack - 90\n defense - 65\n speed - 55\n***************\n";
    cout << endl;
    }
    break;
    case 3:
    if((level <= 6)&&(level >= 4))
    {
    cout<< "(" << d + 7 << ")\n***************\nHades(lvl 6):\n attack - 75\n defense - 70\n speed - 50\n***************\n";
    cout << endl;
    }
    break;
    case 4:
    if((level <= 6)&&(level >= 4))
    {
    cout<< "(" << d + 7 << ")\n***************\nOsiris(lvl 6):\n attack - 70\n defense - 60\n speed - 55\n***************\n";
    cout << endl;
    }
    break;
    default:
    cout << "Can't Happen";
    }
    if((level <= 99)&&(level >= 5))
    {
    cout<< "(" << d + 8 << ")\n***************\nGod(lvl ?):\n attack - ??\n defense - ??\n speed - ??\n***************\n";
    cout << endl;
    }
    cout << ":";
    cin >> opponent;
    if((opponent > 3)||(opponent < 1))
    {
    system("cls");
    cout << "You must enter an integer between 1 and 3!!!!\n\n";
    system("pause");
    system("cls");
    }
    }while((opponent > 3)||(opponent < 1));
    system("cls");
    opponent = opponent + c;
    switch(opponent)
    {
    case 1:
    switch(random3)
    {
    case 1:
    attackop = 0, defenseop = 20, speedop = 0, nameop = "Tree", expo = 40, moneyo = 20;
    break;
    case 2:
    attackop = 0, defenseop = 30, speedop = 0, nameop = "Rock", expo = 60, moneyo = 30;
    break;
    case 3:
    attackop = 0, defenseop = 10, speedop = 0, nameop = "Computer", expo = 20, moneyo = 10;
    break;
    case 4:
    attackop = 0, defenseop = 5, speedop = 0, nameop = "Book", expo = 10, moneyo = 5;
    break;
    }
    break;
    case 2:
    switch(random4)
    {
    case 1:
    attackop = 30, defenseop = 20, speedop = 30, nameop = "Rat", expo = 160, moneyo = 80;
    break;
    case 2:
    attackop = 40, defenseop = 20, speedop = 30, nameop = "Cat", expo = 180, moneyo = 90;
    break;
    case 3:
    attackop = 45, defenseop = 25, speedop = 30, nameop = "Dog", expo = 200, moneyo = 100;
    break;
    case 4:
    attackop = 35, defenseop = 20, speedop = 35, nameop = "Bat", expo = 180, moneyo = 90;
    break;
    }
    break;
    case 3:
    switch(random5)
    {
    case 1:
    attackop = 65, defenseop = 35, speedop = 45, nameop = "Mage", expo = 290, moneyo = 145;
    break;
    case 2:
    attackop = 60, defenseop = 50, speedop = 50, nameop = "Jackel", expo = 320, moneyo = 160;
    break;
    case 3:
    attackop = 65, defenseop = 55, speedop = 45, nameop = "Crusader", expo = 330, moneyo = 165;
    break;
    case 4:
    attackop = 55, defenseop = 45, speedop = 55, nameop = "Wolf", expo = 310, moneyo = 155;
    break;
    }
    break;
    case 4:
    switch(random3)
    {
    case 1:
    attackop = 85, defenseop = 35, speedop = 65, nameop = "Assassin", expo = 330, moneyo = 165;
    break;
    case 2:
    attackop = 75, defenseop = 30, speedop = 70, nameop = "Pacman", expo = 310, moneyo = 155;
    break;
    case 3:
    attackop = 85, defenseop = 35, speedop = 85, nameop = "Man Eating Bunny", expo = 330, moneyo = 165;
    break;
    case 4:
    attackop = 80, defenseop = 40, speedop = 60, nameop = "Warlock", expo = 320, moneyo = 160;
    break;
    }
    break;
    case 5:
    switch(random4)
    {
    case 1:
    attackop = 90, defenseop = 45, speedop = 55, nameop = "Poseidon", expo = 340, moneyo = 170;
    break;
    case 2:
    attackop = 80, defenseop = 55, speedop = 60, nameop = "Minitaur", expo = 350, moneyo = 175;
    break;
    case 3:
    attackop = 85, defenseop = 45, speedop = 70, nameop = "Cyclops", expo = 340, moneyo = 170;
    break;
    case 4:
    attackop = 90, defenseop = 55, speedop = 45, nameop = "Dragon", expo = 340, moneyo = 170;
    break;
    }
    break;
    case 6:
    switch(random5)
    {
    case 1:
    attackop = 105, defenseop = 55, speedop = 70, nameop = "Athena", expo = 400, moneyo = 200;
    break;
    case 2:
    attackop = 110, defenseop = 65, speedop = 65, nameop = "Zeus", expo = 420, moneyo = 210;
    break;
    case 3:
    attackop = 95, defenseop = 70, speedop = 60, nameop = "Hades", expo = 390, moneyo = 195;
    break;
    case 4:
    attackop = 90, defenseop = 60, speedop = 65, nameop = "Osiris", expo = 370, moneyo = 185;
    break;
    }
    break;
    case 7:
    attackop = 110, defenseop = 90, speedop = 80, nameop = "God";
    break;
    default:
    cout << "No One";
    }
    //success rate
    asuccess = speedus - defenseop + 50;
    msuccess = speedus - defenseop + 50;
    cout << "First Round is: " << nameus << " vs. " << nameop << "!!!";
    if(opponent != 7)
    Sleep(1000);
    if(opponent == 7)
    {
    Beep(350,200);
    Beep(300,300);
    Beep(400,200);
    Beep(350,250);
    Beep(400,400);
    Beep(450,600);
    }
    //amount of damage dealt
    a = attackus - defenseop;
    b = magicus - defenseop;
    opac = 100 - defenseus;
    attackop = attackop - defenseus;
    if (attackop < 0)
    attackop = 0;
    else
    attackop = attackop;
    do{
    //Random Number Generator
    int random, random2;
    random = rand() % 100;
    random2 = rand() % 100;
    system("CLS");
    //limitation
    if(asuccess > 100)
    asuccess = 100;
    else
    asuccess = asuccess;
    if(msuccess > 100)
    msuccess = 100;
    else
    msuccess = msuccess;
    //options
    do{
    if((random < 80)&&(random > 75))
    {
    system("cls");
    cout << "Incredible..a mythical sword has fallen from the sky and into your fist.\nYour attack has rose an additional 5 points!\n(only used on THIS round.)\n\n";
    a = a + 5;
    system("pause");
    system("cls");
    }
    if((random < 76)&&(random > 70))
    {
    system("cls");
    cout << "You have just returned from the shoe store, with sneakers faster than lightning!\nYour Speed rose an additional 5 points!\n(only used on THIS round.)\n\n";
    asuccess = asuccess + 5;
    msuccess = msuccess + 5;
    system("pause");
    system("cls");
    }
    if((random < 71)&&(random > 65))
    {
    system("cls");
    cout << "An ancient wizard visited you in your sleep, and gave you a new spell.\nYour magic has rose an additional 5 points!\n(only used on THIS round.)\n\n";
    b = b + 5;
    system("pause");
    system("cls");
    }
    if((random < 66)&&(random > 60))
    {
    system("cls");
    cout << "Athena has just granted you a new sheild!\nYour defense has rose an additional 5 points!\n(only used on THIS round.)\n\n";
    opac = opac - 5;
    system("pause");
    system("cls");
    }
    healus = attackop -5;
    if(healus <= 0)
    healus = 10;
    cout << "What would you like to use?\n\n"
    << "(1)\n***************\nattack: " << asuccess << "% chance of success\n***************\n\n"
    << "(2)\n***************\nmagic: " << msuccess << "% chance of success\n***************\n\n"
    << "(3)\n***************\nheal + " << healus << "hp\n***************\n\n";
    cout << ": ";
    cin >> atk;
    if(atk > 3)
    {
    system("cls");
    cout << "You MUST enter a number between 1 and 3!!!\n\n";
    system("pause");
    system("cls");
    }
    }while(atk > 3);
    system("CLS");
    cout << "Calculating results";
    for(int counter = 0; counter <= 6; counter++)
    {
    Sleep(00);
    cout << ".";
    }
    system("CLS");
    switch(atk)
    {
    case 1:
    if(random<=asuccess)
    {
    if(random<=10)
    {
    cout << "CRITICAL!!!!\n\n\a";
    a = a * 2;
    }
    if(a < 0)
    a = 5;
    cout << "You took away " << a << " health points from the " << nameop << "!\n\n";
    healthop = healthop - a;
    if(healthop < 0)
    healthop = 0;
    cout << "The " << nameop << "'s health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    if(random<=10)
    a = a/2;
    }
    else
    {
    cout << "You missed!\n\n";
    cout << "The " << nameop << "'s health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    }
    break;
    case 2:
    if(random<=msuccess)
    {
    if(random<=10)
    {
    cout << "CRITICAL!!!!\n\n\a";
    b = b * 2;
    }
    if(b < 0)
    b = 5;
    cout << "You took away " << b << " health points from the " << nameop << "!\n\n";
    healthop = healthop - b;
    if(healthop < 0)
    healthop = 0;
    cout << "The " << nameop << "'s health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    if(random<=10)
    b = b/2;
    }
    else
    {
    cout << "You missed!\n\n";
    cout << "The " << nameop << "'s health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    }
    break;
    case 3:
    if(healthus < 100)
    {
    healthus = healthus + healus;
    cout << "Your health rose from " << healthus-healus << ", to " << healthus << "!!!" << endl << endl;
    }
    else
    cout << "Your health is already 100%!!!" << endl << endl;
    break;
    default:
    cout << "Attack Not available";
    }
    system("pause");
    system("cls");
    if(healthop > 0)
    {
    cout << "Opponent is now attacking";
    for(int counter = 6; counter >= 0; counter--)
    {
    Sleep(00);
    cout << ".";
    }
    system("cls");
    if(opac <= random2)
    {
    if(random2<=5)
    {
    cout << "CRITICAL!!!!\n\n\a";
    attackop = attackop + 15;
    }
    if(attackop <= 0)
    attackop = 5;
    if(opponent==1)
    attackop = 0;
    cout << "The opponent took away " << attackop << " health points from you!\n\n";
    healthus = healthus - attackop;
    if(healthus < 0)
    healthus = 0;
    cout << "Opponent's health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    if(random2<=5)
    attackop = attackop - 15;
    }
    else
    {
    cout << "The opponent missed!\n\n";
    cout << "Opponent's health is: " << healthop << endl << "Your health is: " << healthus << endl << endl;
    }
    system("pause");
    }
    }while((healthop >= 1)&&(healthus >= 1));
    system("cls");
    if(healthop >= 1)
    {
    cout << "You were beat by the " << nameop << "!!! Whhaaaahaahaaaaa\n\n";
    Beep(180,500);
    Beep(160,500);
    Beep(140,500);
    Beep(120,500);
    Beep(100,800);
    }
    else if(opponent!=7)
    {
    if(healthus > 0)
    {
    healthus = 100;
    cout << "You have gained " << expo << " experience, and " << moneyo << " money!!!\n\n";
    exp = exp + expo;
    money = money + moneyo;
    cout << "You have " << exp << " experience points!\n";
    if(levelup > exp)
    cout <<	"You need " << levelup - exp << " more exp points to level up.\n";
    cout << "You have $" << money << endl << endl;
    system("pause");
    system("cls");
    }
    if(exp >= levelup)
    {
    c++;
    level++;
    cout << "Congratulations on your victory! You have graduated to level " << level << "!!!.\n\n";
    cout << "Which attribute would you like to increase by 5 points?\n";
    cout << endl << "(1)\n*****\nattack\n*****\n\n(2)\n*****\nmagic\n*****\n\n(3)\n*****\nspeed\n*****\n\n(4)\n*****\ndefense\n*****\n:";
    cin >> atr;
    switch(atr)
    {
    case 1:
    attackus = attackus + 5;
    break;
    case 2:
    magicus = magicus + 5;
    break;
    case 3:
    speedus = speedus + 5;
    break;
    case 4:
    defenseus = defenseus + 5;
    break;
    default:
    defenseus = defenseus;
    break;
    }
    system("cls");
    cout << "Your current Stats are:\nattack - " << attackus << endl << "magic - " << magicus << endl << "speed - " << speedus << endl << "defense - " << defenseus << endl << endl;
    system("pause");
    system("cls");
    levelup = levelup*2;
    }
    }
    system("pause");
    break;
    case 2:
    do{
    do{
    system("cls");
    cout << "You have $" << money << " left.";
    cout << " Which Item would you like to buy?\n\n";
    cout << "(1)\n**********\nRusty Sword(+3 attack) - 300$\n**********\n";
    cout << "(2)\n**********\nSteel Sword(+6 attack) - 500$\n**********\n";
    cout << "(3)\n**********\nGreat Sword(+9 attack) - 700$\n**********\n\n";
    cout << "(4)\n**********\nWooden Shield(+3 defense) - 300$\n**********\n";
    cout << "(5)\n**********\nSmall Shield(+6 defense) - 500$\n**********\n";
    cout << "(6)\n**********\nGaurdian's Shield(+9 defense) - 700$\n**********\n\n";
    cout << "(7)\n**********\nWooden Staff(+3 magic) - 300$\n**********\n";
    cout << "(8)\n**********\nCurved Staff(+6 magic) - 500$\n**********\n";
    cout << "(9)\n**********\nWarlock's Staff(+9 magic) - 700$\n**********\n\n";
    cout << "(10)\n**********\nPatched Loafers(+3 speed) - 300$\n**********\n";
    cout << "(11)\n**********\nRegular Sneakers(+6 speed) - 500$\n**********\n";
    cout << "(12)\n**********\nRunning Shoes(+9 speed) - 700$\n**********\n";
    cout << "(13)\n**********\nExit\n**********\n";
    cin >> item;
    system("cls");
    }while((item > 13)||(item < 1));
    switch(item)
    {
    case 1:
    if(money >= 300)
    {
    cout << "You have gained 3 attack!\n\n";
    money = money - 300;
    attackus = attackus + 3;
    break;
    }
    case 2:
    if(money >= 500)
    {
    cout << "You have gained 6 attack!\n\n";
    money = money - 500;
    attackus = attackus + 6;
    break;
    }
    case 3:
    if(money >= 700)
    {
    cout << "You have gained 9 attack!\n\n";
    money = money - 700;
    attackus = attackus + 9;
    break;
    }
    case 4:
    if(money >= 300)
    {
    cout << "You have gained 3 defense!\n\n";
    money = money - 300;
    defenseus = defenseus + 3;
    break;
    }
    case 5:
    if(money >= 500)
    {
    cout << "You have gained 6 defense!\n\n";
    money = money - 500;
    defenseus = defenseus + 6;
    break;
    }
    case 6:
    if(money >= 700)
    {
    cout << "You have gained 9 defense!\n\n";
    money = money - 700;
    defenseus = defenseus + 9;
    break;
    }
    case 7:
    if(money >= 300)
    {
    cout << "You have gained 3 magic!\n\n";
    money = money - 300;
    magicus = magicus + 3;
    break;
    }
    case 8:
    if(money >= 500)
    {
    cout << "You have gained 3 magic!\n\n";
    money = money - 500;
    magicus = magicus + 6;
    break;
    }
    case 9:
    if(money >= 700)
    {
    cout << "You have gained 3 magic!\n\n";
    money = money - 700;
    magicus = magicus + 9;
    break;
    }
    case 10:
    if(money >= 300)
    {
    cout << "You have gained 3 speed!\n\n";
    money = money - 300;
    speedus = speedus + 3;
    break;
    }
    case 11:
    if(money >= 500)
    {
    cout << "You have gained 3 speed!\n\n";
    money = money - 500;
    speedus = speedus + 6;
    break;
    }
    case 12:
    if(money >= 700)
    {
    cout << "You have gained 3 speed!\n\n";
    money = money - 700;
    speedus = speedus + 9;
    break;
    }
    default:
    speedus = speedus;
    }
    //END Armory
    cout << "Would you like to exit? (y or n): ";
    cin >> exit;
    system("cls");
    }while(exit != 'y');
    break;
    default:
    cout << "Please select 1 or 2!!!";
    }
    }while((healthus > 0)&&(opponent!=7));
    }while((healthus >= 1)&&(opponent!=7));
    if(healthus >= 1)
    {
    cout << "OMG!! YOU WON!!! You are better than god...congrats!\n";
    cout << " ******** ";
    cout << " ************ ** ";
    cout << " ************** ****** ";
    cout << " *********************** ";
    cout << " *********************** ";
    cout << " **************** ***** ";
    cout << " ************** *** ";
    cout << " ************ * ";
    cout << " ******** *** * * ";
    cout << " **** * * * ";
    cout << " ** * * * ";
    cout << " **** * * * ";
    cout << " * ";
    cout << " * ";
    cout << " * * * ";
    cout << " * * * ";
    cout << " * * * ";
    cout << " * ";
    cout << " * ";
    cout << " * ";
    Beep(100,100);
    Beep(120,100);
    Beep(140,100);
    Beep(160,100);
    Beep(180,100);
    Beep(200,100);
    Beep(220,100);
    Beep(240,100);
    Beep(260,100);
    Beep(280,100);
    Beep(300,100);
    Beep(320,100);
    Beep(340,100);
    Beep(360,100);
    Beep(380,100);
    Beep(400,100);
    Beep(420,100);
    Beep(440,100);
    Beep(460,100);
    Beep(480,100);
    Beep(500,100);
    Beep(600,300);
    }
    }

