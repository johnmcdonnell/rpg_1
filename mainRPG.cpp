/*
John McDonnell & Delwyn Monagan
K28_Comp2
Games Development CA 2
RPG Text Based Game
Final Version
Due Date: 22-Nov-2014
*/

/*
Issues: None.
Game has been tested and all movement between rooms is correct
Items can be collected and dropped
Sounds indicates when item is collected or dropped
*/

// main.cpp
#include "room.h"
#include "Player.h"
#include "Item.h"
#include <iostream>
#include <fstream>
#include<string>
#include<vector>
#include <ctime> // Needed for the true randomization
#include <cstdlib> 
#include <windows.h>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

//trap door function - if random num is triggered game is over.
int trapDoor()
{
	cout << "Game Over" << endl;
	cout << "Your Dead!!!!" << endl;
	cout << "You Fell into a Trap Door!!" << endl;
	Beep(900, 500); // sound for death
	Beep(800, 500);
	Beep(700, 500);
	Beep(300, 500);
	Beep(200, 500);
	Beep(100, 500);
	system("PAUSE");
	return -3;
}
// random number is generated between 1 & 10
int randomNumber()
{
	int xRan;
	srand(time(0)); // This will ensure a really randomized number by help of time.

	xRan = rand() % 10 + 1; // Randomizing the number between 1-10.
	if (xRan > 9) // if greater than call door i.e. 0.1 chance
	{
		trapDoor();
	}
	else
	{
		return 0;
	}
}
// function to move into eachroom.
int getDirection(int currentRoom, vector<Room> &floorPlan,vector<string> &move)
{
	string choice;
	int x = 0;
		cout << endl <<"|Where would you like to move" << endl << endl << "Keywords: moveNorth , moveSouth, moveEast, moveWest" << endl << endl;
		cin >> choice;

	if (choice == move[0])
	{
		if (floorPlan[currentRoom].getNorth() != -1)// get current room in floorplan, go north.
		{
			x =randomNumber(); //calling random number
			if (x == -3)
			{
				return x;
			}
			return currentRoom = floorPlan[currentRoom].getNorth(); // return new room north
		}
		else
		{
			cout << "|You can't travel north!!!!!!" << endl;
			return currentRoom = getDirection(currentRoom, floorPlan, move); // stay in the current room
		}
	}
	//same as above but for south movement
	else if (choice == move[1])
	{
		if (floorPlan[currentRoom].getSouth() != -1)
		{
			x = randomNumber(); // call random number
			if (x == -3)
			{
				return x;
			}
			return currentRoom = floorPlan[currentRoom].getSouth();
		}
		else if (floorPlan[currentRoom].getSouth() == -2)
		{
			return currentRoom = -2; // exit the castle
		}
		else
		{
			cout << "|You can't travel south!!!!!!" << endl;
			return currentRoom = getDirection(currentRoom, floorPlan,move); // say in current room
		}
	}
	else if (choice == move[2])
	{
		if (floorPlan[currentRoom].getEast() != -1)
		{
			x = randomNumber();
			if (x == -3)
			{
				return x;
			}
			return currentRoom = floorPlan[currentRoom].getEast();
		}
		else
		{
			cout << "|You can't travel east!!!!!!" << endl;
			return currentRoom = getDirection(currentRoom, floorPlan, move);
		}
	}
	else if (choice == move[3])
	{
		if (floorPlan[currentRoom].getWest() != -1)
		{
			x = randomNumber();
			if (x == -3)
			{
				return x;
			}
			return currentRoom = floorPlan[currentRoom].getWest();
		}
		else
		{
			cout << "|You can't travel west!!!!!!" << endl;
			return currentRoom = getDirection(currentRoom, floorPlan, move);
		}
	}
	else
	{
		cout << "|Input not valid, please try again" << endl;
		return currentRoom = getDirection(currentRoom, floorPlan, move);
	}
}


int main()
{
	// declaring vectors and variables
	vector<Room> floorPlan;
	vector<Item> itemPlan;
	vector<string> move;
	vector<Item> inventory;
	int currentRoom;
	string name;
	string behaviour;
	bool end = false;

	// Rooms - Room - Description - Exits - Positions
	Room r0("Lobby", "You enter the main door and step inside the Lobby. The room is cold and damp with just candles softly lighting the square room. Pictures hang on the two side walls and a crack in the ceiling above the middle of the north wall allows a trickle of water to flow down to the floor. The water pools near the base of the wall, and a rivulet runs along the wall an out into the hall. The water smells fresh. But it looks like nobody has been here in a while.", "The door to the North exits to the grand hall.The Door to the South Exits the Castle", 0, 1, -2, -1, -1);
	Room r1("Hall", "It is a long dimly lit hallway. You get the faint trace of rot and decay, as you enter the room the wooden floor creaks and echo�s throughout the long hallway. Thick cobwebs fill the corners of the room, and wisps of webbing hang from the ceiling and waver in a wind you can barely feel. One corner of the ceiling has a particularly large clot of webbing within which a large medieval statue is tangled, the statue looks unstable and may fall off the wall at any time.", "The door to the South exits to the lobby. The door to the North exits to the kitchen.The door to the West exits to the master Bedroom.The door to the East exits to the guest Bedroom.", 1, 5, 0, 2, 3);
	Room r2("Bedroom", "It is a small bedroom. The scent of mold is strong and at times stomach turning. It looks like somebody was busy looking for something, the room is completely ransacked and the floor is covered in splinters of glass, which has come from the smashed mirror on the north wall.", "Exits: The door to the West exits to the Grand Hall.", 2, -1, -1, -1, 1);
	Room r3("Master Room", "It is a large bedroom. The smell of rot has gotten stronger, and there is scratching coming from the room connected to the Master Room. This room is also ransacked and the creaking floorboards are covered in dry blood. Family pictures are hung through the room and cobwebs hang from the once magnificent chandler.", "The door to the South exits to the Bathroom. The door to the East exits to the guest Grand Hall.", 3, -1, 4, 1, -1);
	Room r4("Bathroom", "It is a brightly lit room. The tiles are stained with blood. The smell of rot is overwhelming. Rats inside the room shriek when they hear the door open, then they run in all directions from a putrid corpse lying in the centre of the floor. As these creatures crowd around the edges of the room, seeking to crawl through a hole in one corner, they fight one another. The stinking corpse in the middle of the room looks human, but the damage both time and the rats have wrought are enough to make determining  by appearance an extremely difficult task at best.", "The door to the North exits to the master Bedroom.", 4, 3, -1, -1, -1);
	Room r5("Kitchen", "The moment the door opens you feel a breeze that sends shivers down your spine. The cold tiles have mud and blood foot prints all over them, the large coloured glass window in the north of the room has been broken, the wind whistles through the window. All the presses and draws have been thrown on the black and white tiles. It seems like someone was looking for something important. The smell of rotten food from the presses and fridge is stomach turning.", "The door to the North exits to the Dining Room. The door to the South exits to the Grand Hall. The door to the East exits to the Living Room. The door to the West exits to the Store Room.", 5, 7, 1, 8, 6);
	Room r6("Store Room", "The damp and rotting smell hits you the moment the door slides open. The Store Room looks like it was once filled with fresh food and veg, but this places hasn�t been cleaned in weeks. When the lights are turned on it reveals a hole in the south wall, something has been taking but what?", "The door to the East exits to the Kitchen.", 6, -1, -1, 5, -1);
	Room r7("Dining Room", "This room smells strange, no doubt due to the weird sheets of black slime that drip from cracks in the ceiling and spread across the floor. The slime seeps from the shattered stone of the ceiling at a snail�s crawl, forming a mess of dangling walls of gook. As you watch, a bit of the stuff separates from a sheet and drops to the ground with a wet plop. The oak furniture has seen better days and pictures hang from the red walls which once made this room special. ", "The door to the South exits to the Kitchen.", 7, -1, 5, -1, -1);
	Room r8("Living Room", "The scent of earthy decay assaults your nose upon peering through the open door to this room. Smashed bookcases and their sundered contents litter the floor. Paper rots in mold-spotted heaps, and shattered wood grows white fungus. The Living Room was once a room to be proud of but now it�s in need of some tlc.", "The door to the South exits to the Gaming Room.The door to the West exits to the Kitchen.", 8, -1, 9, -1, 5);
	Room r9("Gaming Room", "Once the granddaddy of entertainment rooms, this over the top room is now dormant. Nothing but smashed glass and shattered desks lay on the hard wooden floor. Fungus grows on the once cream sofa which has a gaping hole in its side. Maybe someone found something. Family pictures scatter the huge square room.", "The door to the North exits to the Living Room.", 9, 8, -1, -1, -1);

	// items - Item - Description
	Item i0("Note", " There is a note on the floor; it's from the owner's mother who is writing to her child. In the letter she is concerned as she hasn't heard from the owner in over three weeks");
	Item i1("Keys", "There are a set of keys on the wooden stand");
	Item i2("Bullets", " Bullet casings cover the floor.");
	Item i3("Knife", "Under the bed there is a knife covered in what looks to be dried blood.");
	Item i4("No item", "There are no items in this room.");
	Item i5("Knives", "There is a set of knives, one seems to be missing.");
	Item i6("License", "There is a driver's licence in the north corner, it has a dried blood finger print on it.");
	Item i7("Hat", "There is a baseball cap hanging on a stand, which could be the murders.");
	Item i8("Bullets", "More bullet casings scatter the floor, these look to be different from the ones in the bedroom.");
	Item i9("Key", "There is a key under the once cream sofas. This should open the bathroom door");

	// pushing rooms into floorplan vector
	floorPlan.push_back(r0);
	floorPlan.push_back(r1);
	floorPlan.push_back(r2);
	floorPlan.push_back(r3);
	floorPlan.push_back(r4);
	floorPlan.push_back(r5);
	floorPlan.push_back(r6);
	floorPlan.push_back(r7);
	floorPlan.push_back(r8);
	floorPlan.push_back(r9);

	// pushing items into itemplan vector
	itemPlan.push_back(i0);
	itemPlan.push_back(i1);
	itemPlan.push_back(i2);
	itemPlan.push_back(i3);
	itemPlan.push_back(i4);
	itemPlan.push_back(i5);
	itemPlan.push_back(i6);
	itemPlan.push_back(i7);
	itemPlan.push_back(i8);
	itemPlan.push_back(i9);

	// pushing moves into move vector
	move.push_back("moveNorth");
	move.push_back("moveSouth");
	move.push_back("moveEast");
	move.push_back("moveWest");

	// game intro
	cout << "The Following Game was created by Delwyn Monaghan and John McDonnell as part of a paired assignment for their Games Development module" << endl << endl << endl;
	cout << "You're a police detective who has just been assigned the task of finding and apprehending the mayor's murderer. You have to do it quickly in order to avoid any bad publicity. But, you're not alone. Everywhere you go and with everything you do, there is danger lurking just around the corner." << endl << endl << endl;
	cout << "Detective: The Castle Mystery is a text - based role playing game set in Ireland in the early 1990s.With the mayors murder unannounced Detective Carey Mahoney is facing a battle against time to find clues to Mayor Adam West�s murder.Detective Mahoney is not alone, throughout the vast Castle traps await, and danger is not far away. " << endl << endl << endl << endl;
	
	Beep(100, 500); // sound for intro
	Beep(200, 500);
	Beep(300, 500); 
	Beep(400, 500); 
	Beep(500, 500); 
	Beep(400, 500); 
	Beep(300, 500);
	Beep(200, 500); 
	Beep(300, 500); 
	Beep(600, 500);
	Beep(800, 500);
	Beep(400, 500);
	Beep(700, 500);
	Beep(100, 500);

	// asking for player name
	cout << "Please enter name" << endl;
	cin >> name;

	Player mainPlayer(name);
	cout << "" << endl;
	cout << "" << endl;
	cout << "Hello " + mainPlayer.getName() << endl << endl << endl << "Prepare yourself, danger lies ahead" << endl << endl;
	cout << "" << endl;
	cout << "" << endl;

	// setting room to lobby
	currentRoom = 0;

	// Room 1 , description, exits, items, and item description
	cout << "You Entered The: " << floorPlan[currentRoom].getName() << endl << endl;
	cout << "Description: " << floorPlan[currentRoom].getDescription() << endl << endl;
	cout << "Exits: " << floorPlan[currentRoom].getExits() << endl << endl;
	cout << "Item in the room: " << itemPlan[currentRoom].getItemName() << endl << endl;
	cout << "Item description: " << itemPlan[currentRoom].getItemDescription() << endl << endl;

	// kepping going into each room gathering the info until end = true
	while (end != true)
	{
		cout << "Make a choice." << endl << endl;

		cout << "Keywords: move(so you can choose which direction to go), pickup(pickup item in room if any), drop(drop item from inventory), look(look around room), inventory(display all items in inventory), exit (exit the game)" << endl << endl;;
		cin >> behaviour;
		if (behaviour == "move")
		{
			currentRoom = getDirection(currentRoom, floorPlan, move);
			if (currentRoom == -3)
			{
				end = true;
				return(0);
			}
			else
			{
				cout << "Your in the: " << floorPlan[currentRoom].getName() << endl << endl;
				cout << "Description: " << floorPlan[currentRoom].getDescription() << endl << endl;
				cout << "Exits: " << floorPlan[currentRoom].getExits() << endl << endl;
				cout << "Item in the room: " << itemPlan[currentRoom].getItemName() << endl << endl;
				cout << "Item description: " << itemPlan[currentRoom].getItemDescription() << endl << endl;
			}
		}
		else if (behaviour == "pickUp")
		{
			if (itemPlan[currentRoom].getItemName() == "No item")
			{
				cout << "No Item in this room";
			}
			else if (mainPlayer.getInventory().size() <= 2)
			{		// adding an item into inventory
				mainPlayer.addItem(itemPlan[currentRoom], mainPlayer.getCount());
				cout << "Item " + itemPlan[currentRoom].getItemName() + " added" << endl << endl;
				Beep(523, 500); // sound for pick up
				Beep(600, 500);
				itemPlan[currentRoom].removeItem(); // removing item from current room
			}
			else if (mainPlayer.getInventory().size() >= 2)
			{
				cout << "No room in inventory for new Item" << endl << endl; // inventory limit 5 has been reached
			}

		}
		else if (behaviour == "drop") // drop an item
		{
			string choice;
			cout << "What item would you like to drop" << endl << endl;;
			cin >> choice;
			for (int i = 0; i < mainPlayer.getInventory().size(); i++)
			{
				if (choice == mainPlayer.getInventory()[i].getItemName()) // drop item user selected
				{
					mainPlayer.setCount(mainPlayer.removeItem(i, mainPlayer.getCount()));
					Beep(200, 500); // sound for drop
					Beep(800, 500);
					break;
				}
				else
				{
					cout << "No item by that name" << endl << endl; // item is not there
				}
			}

		}
		else if (behaviour == "exit") // end game
		{
			end = true;
		}
		else if (behaviour == "inventory")
		{
			vector<Item> arr = mainPlayer.getInventory();
			for (int i = 0; i < arr.size(); i++)
			{	// slot i = players inventory item
				cout << "Slot " << i << " " << mainPlayer.getInventory()[i].getItemName() << endl;
			}
		}
		else if (behaviour == "look")
		{	// check what is in the room
			cout << "Your in the: " << floorPlan[currentRoom].getName() << endl << endl;
			cout << "Description: " << floorPlan[currentRoom].getDescription() << endl << endl;
			cout << "Exits: " << floorPlan[currentRoom].getExits() << endl << endl;
			cout << "Item in the room: " << itemPlan[currentRoom].getItemName() << endl << endl;
			cout << "Item description: " << itemPlan[currentRoom].getItemDescription() << endl << endl;
		}
		else if (behaviour == "Keywords")
		{	// display games keywords
			cout << "Keywords: move(so you can choose which direction to go), pickup(pickup item in room if any), drop(drop item from inventory), look(look around room), inventory(display all items in inventory), exit (exit the game)" << endl << endl;
		}
	}
	return 0;
}


